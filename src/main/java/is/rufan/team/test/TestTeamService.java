package is.rufan.team.test;

import is.rufan.team.domain.Team;
import is.rufan.team.service.TeamService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import java.util.List;

/**
 * Created by Snaebjorn on 10/22/2015.
 */
public class TestTeamService {
    public static void main(String[] args) {
        new TestTeamService();
    }

    public TestTeamService() {
        ApplicationContext ctx = new FileSystemXmlApplicationContext("classpath:teamapp.xml");
        TeamService teamService = (TeamService) ctx.getBean("teamService");

        List<Team> teams = teamService.getTeams();
        for (Team t : teams) {
            System.out.println("Team: " + t.getDisplayName()
                + " Venue: " + t.getVenue().getName());
        }

        Team team = teamService.getTeam(6140);
        System.out.println(team);
    }
}
