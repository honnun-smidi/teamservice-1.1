package is.rufan.team.data;

import is.rufan.team.domain.Game;

import java.util.List;

/**
 * Created by Snaebjorn on 10/21/2015.
 */
public interface GameDataGateway {
    public void addGame(Game game);
    public Game getGame(int gameId);
    public List<Game> getGames();
}
