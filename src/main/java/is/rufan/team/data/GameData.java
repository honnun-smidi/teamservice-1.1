package is.rufan.team.data;

import is.rufan.team.domain.Game;
import is.ruframework.data.RuData;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Snaebjorn on 10/21/2015.
 */
public class GameData extends RuData implements GameDataGateway {
    public void addGame(Game game) {
        SimpleJdbcInsert insertPlayer =
            new SimpleJdbcInsert(getDataSource())
                .withTableName("games");

        Map<String, Object> gameParameters = new HashMap<String, Object>(5);
        gameParameters.put("gameId", game.getGameId());
        gameParameters.put("startTime", game.getStartTime());
        gameParameters.put("teamHomeId", game.getTeamHome().getTeamId());
        gameParameters.put("teamAwayId", game.getTeamAway().getTeamId());
        gameParameters.put("venueId", game.getVenue().getVenueId());

        try {
            insertPlayer.execute(gameParameters);
        } catch (DataIntegrityViolationException divex) {
            log.warning("Duplicate entry");
        }
    }

    public Game getGame(int gameId) {
        String sql = "select * from games where id = ?";
        JdbcTemplate queryGame = new JdbcTemplate(getDataSource());
        Game game = queryGame.queryForObject(sql, new Object [] { gameId },
                new GameRowMapper());
        return game;
    }

    public List<Game> getGames() {
        String sql = "select * from games";
        JdbcTemplate queryGame = new JdbcTemplate(getDataSource());

        List<Game> games = queryGame.query(sql,
            new GameRowMapper());

        return games;
    }
}
