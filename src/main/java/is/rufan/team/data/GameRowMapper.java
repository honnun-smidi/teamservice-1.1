package is.rufan.team.data;

import is.rufan.team.domain.Game;
import is.rufan.team.domain.Team;
import is.rufan.team.domain.Venue;
import is.rufan.team.service.TeamService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by Snaebjorn on 10/21/2015.
 */
public class GameRowMapper implements RowMapper<Game> {
    private TeamService teamService;

    public GameRowMapper() {
        ApplicationContext teamContext = new FileSystemXmlApplicationContext("classpath:teamapp.xml");
        teamService = (TeamService) teamContext.getBean("teamService");
    }

    public Game mapRow(ResultSet rs, int i) throws SQLException {
        Game game = new Game();
        game.setGameId(rs.getInt("gameId"));
        game.setStartTime(rs.getDate("startTime"));
        Team teamHome = teamService.getTeam(rs.getInt("teamHomeId"));
        Team teamAway = teamService.getTeam(rs.getInt("teamAwayId"));
        Venue venue = teamHome.getVenue();
        game.setTeamHome(teamHome);
        game.setTeamAway(teamAway);
        game.setVenue(venue);

        return game;
    }
}
