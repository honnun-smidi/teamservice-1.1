drop table games;
create table games
(
  gameId int primary key NOT NULL,
  startTime datetime,
  teamHomeId int NOT NULL,
  teamAwayId int NOT NULL,
  venueId int NOT NULL
)